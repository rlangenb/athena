/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/


#include "../ActsKalmanFitter.h"
#include "../ActsReFitterAlg.h"
#include "../ActsCompareTrackAlg.h"

DECLARE_COMPONENT( ActsKalmanFitter )
DECLARE_COMPONENT( ActsReFitterAlg )
DECLARE_COMPONENT( ActsCompareTrackAlg )